/*
 * Kostak A Simple Container Example
 *
 * Copyright (C) 2020 Didiet Noor
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include "common.h"
#include "mount_ns.h"

static inline void mount_procfs() {
  if (-1 == mkdir("/proc", 0555) && errno != EEXIST) {
    DIE("Failed to mkdir /proc: %m");
  }

  if (-1 == mount("proc", "/proc", "proc", 0, "")) {
    DIE("Failed to mount proc: %m");
  }
}

void change_root(const char *newroot) {

  /* 
   * 1. Prepare root and mount as slave this will
   *    make any mount events won't propagate to the parent process
   */
  if (-1 == mount("", "/", "", 
    MS_REC
    | MS_SLAVE
    , NULL)) {
    DIE("Failed remounting rootfs as slave: %m");
  }

  /*
   * 2. Create and mount newroot to temporary directory /tmp/rootfs.XXXXXX
   *    which XXXXXX is unique string
   */ 

  char mount_dir[PATH_MAX] = "/tmp/rootfs.XXXXXX";
  if (NULL == mkdtemp(mount_dir)) {
    DIE("Failed creating mount point at temp: %m");
  }

  if(-1 == mount(newroot, mount_dir, "", MS_BIND | MS_REC, NULL )) {
    DIE("Failed mounting %s to %s: %m", newroot, mount_dir);
  }

  /* 
   * 3. Create empty directory for put_old and then pivot the
   *    root to the newly-mounted directory
   */
  char put_old[PATH_MAX];
  snprintf(put_old, PATH_MAX, "%s/.put_old.XXXXXX", mount_dir);
  if(NULL == mkdtemp(put_old)) {
    DIE("Failed creating temporary file in %s: %m", mount_dir);
  }

  if(-1 == syscall(SYS_pivot_root, mount_dir, put_old)) {
    DIE("Failed at pivoting root: %m");
  }

  if (-1 == chdir("/")) {
    DIE("Failed at chdir to new root: %m");
  }

  char base_old_dir[PATH_MAX];
  char *old_dir_path = basename(put_old);

  strncpy(base_old_dir, old_dir_path, PATH_MAX);

  mount_procfs();

  if (-1 == umount2(base_old_dir, MNT_DETACH)) {
    DIE("Failed at unmounting %s: %m", put_old);
  }

  if (-1 == rmdir(base_old_dir)) {
    DIE("Failed at removing old directory: %m");
  }
}
