# Container From Scratch Source Code

This is the container from scratch source code I'm using to present on Docker Online Meetup. This small container
runtime utilises namespaces.

## Compiling


### Prerequisite

You'd need to run Linux. This program make use of [CMake Build
Tools](http://cmake.org) and some test code is running Python 3. The source
code doesn't need latest and greatest software to be installed as long as it
conforms C99 standard in which GCC has been compatible since GCC 4.5. This also
has no 3rd party software dependencies.

#### Debian Based System

On Debian based system you'd need to install these packages. This command runs
on debian buster but may run on Ubuntu and such.

```bash
apt-get install -y build-essential make cmake man-db python3 git curl
```

#### Red Hat Based System

On Red Hat based system you'd need to install these packages. This command runs
on CentOS 8 but may run on RHEL or Fedora

```bash
dnf -y install epel-release
dnf -y install gcc gdb cmake make kernel-devel kernel-headers git man-pages curl
```

#### Other Linux Distributions

If you're using other distribution or even [Linux From
Scratch](http://www.linuxfromscratch.org) ;), you'd need at least to have these
packages installed

- Linux Kernel Version 3.10 and up and its headers. This is because we don't use [user
  namespaces](http://man7.org/linux/man-pages/man7/user_namespaces.7.html) yet
  but kernel 4.x is recommended.
- GCC 4.5 and up.
- GDB
- CMake 2.8 and up.
- Make

### Compiling

Create a subdirectory called `build`

```bash
$ mkdir build
$ cd build
```
And run cmake to configure and `make` to build.

```bash
$ cmake ..
$ make
```

There will be `kostak` binary. Try to invoke without any parameters

```bash
$ ./kostak
```

### Running

You'd need to create a rootfs inside the same directory where `kostak` binary is placed. The easiest way is just use
alpine linux mini rootfs.

```bash
$ mkdir rootfs
$ curl -sSL http://dl-cdn.alpinelinux.org/alpine/v3.11/releases/x86_64/alpine-minirootfs-3.11.5-x86_64.tar.gz  | \
  gzip -dc - | tar xvf -
```

And then run it as root

```bash
$ sudo ./kostak /bin/sh
```

This will run Alpine Linux's shell inside the container. 


#### Another way of creating rootfs

To create rootfs there are another way too.

##### RedHat/Fedora based system

Use DNF

```bash
  sudo dnf -y \
  --installroot=$PWD/rootfs \
  --releasever=8 install \
  procps-ng \
  python3 \
  which \
  iproute \
  net-tools
```
This will create a rootfs with software from fedora.


#### Debian based system

Use debootstrap

```bash
$ debootstrap --include=python3 --arch=amd64 bionic $PWD/rootfs http://archive.ubuntu.com/ubuntu/
```

#### From Scratch™

If you're like me who likes to build rootfs by yourself. You can do it using [buildroot](http://buildroot.org). You
shouldn't include linux kernel and boot system onto your rootfs.

### License

This software is licensed with GNU Affero General Public License Version 3

```
    Kostak A Simple Container Example

    Copyright (C) 2020 Didiet Noor

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
